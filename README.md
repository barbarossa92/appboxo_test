# Appboxo Test

Simple overview of use/purpose.

## Description

Todos and categories REST API

## Getting Started

### Dependencies

* Docker, docker-compose

### Installing

#### Local
```
docker-compose -f local.yml up -d --build
```
#### Production
```
docker-compose -f prod.yml up -d --build
```
### Testing
```
docker-compose -f local.yml run --rm backend python manage.py test
```

## Author

[Daniyar Tynybekov](https://www.linkedin.com/in/daniyar-tynybekov-654400129/)

## Version History

* 0.1
    * Initial Release
    