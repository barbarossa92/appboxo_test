import json
import logging
from cryptography.fernet import Fernet


def encrypt(data, key):
    d = json.dumps(data)
    fern = Fernet(key)
    return fern.encrypt(d.encode('utf-8'))


def decrypt(data, key):
    fern = Fernet(key)
    d = fern.decrypt(data)
    return json.loads(d)
