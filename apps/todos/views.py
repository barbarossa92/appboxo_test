from django.shortcuts import render
from rest_framework import viewsets
from .models import Todo
from .serializers import (
    TodoSerializer,
    TodoEncryptSerializer
)
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework.response import Response
from rest_framework import status
from django.conf import settings
# Create your views here.


class TodoViewSet(viewsets.ModelViewSet):
    lookup_field = 'pk'
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    crypt_serializer_class = TodoEncryptSerializer

    @method_decorator(cache_page(60 * 60), name='todos-list')
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @method_decorator(cache_page(60 * 60), name='todos-detail')
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        data = self.get_decrypted_data(request)
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        data = self.get_decrypted_data(request)
        serializer = self.get_serializer(instance, data=data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    @classmethod
    def check_encrypt_in_headers(cls, request):
        return settings.ENCRYPT_HEADER_KEY in request.META

    def get_decrypted_data(self, request):
        if self.check_encrypt_in_headers(request):
            serializer = self.crypt_serializer_class(data=request.data)
            serializer.is_valid(raise_exception=True)
            return serializer.decrypted_data
        return request.data
