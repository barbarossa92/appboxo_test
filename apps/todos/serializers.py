import logging
from rest_framework import serializers
from .models import Todo
from apps.utils.crypt import decrypt, encrypt
from django.conf import settings


class TodoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Todo
        exclude = ()
        extra_kwargs = {'created_at': {'read_only': True}}

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.encrypt:
            return TodoDecryptSerializer(instance=instance, context=data).data
        return data


class TodoEncryptSerializer(serializers.Serializer):
    payload = serializers.CharField()
    __decrypted_data = {}

    def validate(self, attrs):
        payload = attrs.get('payload')
        try:
            decrypted_data = decrypt(payload.encode('utf-8'), settings.ENCRYPTION_KEY)
        except Exception as err:
            logging.error(err)
            raise serializers.ValidationError('Invalid payload.')
        self.__decrypted_data = decrypted_data
        return attrs

    @property
    def decrypted_data(self):
        return self.__decrypted_data


class TodoDecryptSerializer(serializers.Serializer):
    payload = serializers.SerializerMethodField()
    encrypt = serializers.SerializerMethodField()

    class Meta:
        model = Todo
        fields = ('payload', 'encrypt')

    def get_payload(self, obj):
        return encrypt(self.context, settings.ENCRYPTION_KEY)

    def get_encrypt(self, obj):
        return obj.encrypt
