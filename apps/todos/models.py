from django.db import models
from apps.categories.models import Category
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.core.cache import cache
# Create your models here.


class Todo(models.Model):
    class Meta:
        verbose_name_plural = 'Todo tasks'
        verbose_name = 'Tod task'

    title = models.CharField('Title', max_length=255)
    description = models.TextField(null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True)
    encrypt = models.BooleanField(default=False)
    begin_date = models.DateTimeField('Begin date', null=True, blank=True)
    end_date = models.DateTimeField('End date', null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.title


@receiver([post_save, post_delete], sender=Todo)
def clear_cache(sender, instance, **kwargs):
    cache.delete('todos-list')
    cache.delete('todos-detail')
