from django.test import TestCase
from django.conf import settings
from django.urls import reverse
from rest_framework import status
from apps.utils.crypt import encrypt
# Create your tests here.


class TodoTest(TestCase):

    def setUp(self):
        self.base_data = {
            'title': 'Some title',
            'description': 'Some desc',
            'category': '',
            'encrypt': True
        }

    def test_should_create_todo_and_return_201(self):
        resp = self.client.post(reverse('api:v1:todos-list'), data=self.base_data)
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)

    def test_should_return_bad_request_error(self):
        headers = {settings.ENCRYPT_HEADER_KEY: 1}
        resp = self.client.post(reverse('api:v1:todos-list'), data=self.base_data, **headers)
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)

    def test_should_send_encrypt_data_and_create_todo(self):
        headers = {settings.ENCRYPT_HEADER_KEY: True}
        data = {'payload': encrypt(self.base_data, settings.ENCRYPTION_KEY).decode('utf-8')}
        resp = self.client.post(reverse('api:v1:todos-list'), data=data, **headers)
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)

    def test_should_send_invalid_payload_and_return_bad_request_error(self):
        headers = {settings.ENCRYPT_HEADER_KEY: True}
        data = {'payload': encrypt(self.base_data, 'LRknQq4aDgxTdSgbtrgmFHcCrIj3jwNp-F7Ji5wE0Hk=').decode('utf-8')}
        resp = self.client.post(reverse('api:v1:todos-list'), data=data, **headers)
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
