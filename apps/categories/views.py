from django.shortcuts import render
from rest_framework import viewsets
from .models import Category
from .serializers import CategorySerializer
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
# Create your views here.


class CategoriesViewSet(viewsets.ModelViewSet):
    lookup_field = 'pk'
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    @method_decorator(cache_page(60 * 60), name='categories-list')
    def list(self, request, *args, **kwargs):
        self.queryset = Category.objects.filter(parent_isnull=True)
        return super().list(request, *args, **kwargs)

    @method_decorator(cache_page(60 * 60), name='categories-detail')
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)