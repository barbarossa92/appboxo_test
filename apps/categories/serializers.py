from rest_framework import serializers
from .models import Category


class CategorySerializer(serializers.ModelSerializer):
    SUB_CATEGORIES = 'sub_categories'

    class Meta:
        model = Category
        exclude = ()

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data[self.SUB_CATEGORIES] = CategorySerializer(instance.category_set.all(), many=True).data
        return data
