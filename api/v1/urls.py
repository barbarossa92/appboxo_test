from rest_framework.routers import DefaultRouter
from apps.categories.views import CategoriesViewSet
from apps.todos.views import TodoViewSet


router = DefaultRouter()
router.register('categories', CategoriesViewSet, basename='categories')
router.register('todos', TodoViewSet, basename='todos')

urlpatterns = router.urls
