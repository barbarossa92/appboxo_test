#!/bin/bash

python manage.py migrate
python manage.py collectstatic --no-input
gunicorn appboxo_test.wsgi --bind 0.0.0.0:$PORT --workers $WORKERS_NUM --timeout 120